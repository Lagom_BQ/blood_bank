import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:donor_app/services/usermanagement.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  String _email, _password, _fname, _lname, _town, _country, _bType, _age;
  final mainReference = FirebaseDatabase.instance.reference();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Center(
        child: Container(
          padding: EdgeInsets.all(70.0),
          color: Colors.white,
          child: ListView(
            children: <Widget>[
              Image.asset(
                'assets/logo.png',
                fit: BoxFit.fitWidth,
              ),
              TextField(
                decoration: InputDecoration(hintText: "First name"),
                onChanged: (value) {
                  setState(() {
                    _fname = value;
                  });
                },
              ),
              SizedBox(
                height: 15.0,
              ),
              TextField(
                decoration: InputDecoration(hintText: "Last name"),
                onChanged: (value) {
                  setState(() {
                    _lname = value;
                  });
                },
              ),
              TextField(
                decoration: InputDecoration(hintText: "Age"),
                onChanged: (value) {
                  setState(() {
                    _age = value;
                  });
                },
              ),
              SizedBox(
                height: 15.0,
              ),
              TextField(
                decoration: InputDecoration(hintText: "City/ Town"),
                onChanged: (value) {
                  setState(() {
                    _town = value;
                  });
                },
              ),
              SizedBox(
                height: 15.0,
              ),
              TextField(
                decoration: InputDecoration(hintText: "Country"),
                onChanged: (value) {
                  setState(() {
                    _country = value;
                  });
                },
              ),
              SizedBox(
                height: 15.0,
              ),
              TextField(
                decoration: InputDecoration(hintText: "Blood Type"),
                onChanged: (value) {
                  setState(() {
                    _bType = value.toUpperCase();
                  });
                },
              ),
              SizedBox(
                height: 15.0,
              ),
              TextField(
                decoration: InputDecoration(hintText: "Email"),
                onChanged: (value) {
                  setState(() {
                    _email = value;
                  });
                },
              ),
              SizedBox(
                height: 15.0,
              ),
              TextField(
                decoration: InputDecoration(hintText: "Password"),
                onChanged: (value) {
                  setState(() {
                    if (_password.length > 6)
                      _password = value;
                    else
                      Fluttertoast.showToast(
                          msg: "Too short",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIos: 1);
                  });
                },
                obscureText: true,
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    child: Text("Sign Up"),
                    color: Colors.red,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    textColor: Colors.white,
                    elevation: 7.0,
                    onPressed: () {
                      Firestore.instance
                          .collection("/UserData")
                          .document()
                          .setData({
                        'FirstName': _fname,
                        'LastName': _lname,
                        'Age': _age,
                        'City/Town': _town,
                        'Country': _country,
                        'BloodType': _bType,
                      });

                      FirebaseAuth.instance
                          .createUserWithEmailAndPassword(
                              email: _email, password: _password)
                          .then((signedInUser) {
                        UserManagement().storeNewUser(signedInUser, context);
                      }).catchError((e) {
                        print(e);
                      });
                      Navigator.pushReplacementNamed(context, '/homepage');
                    },
                  )
                ],
              ),
              SizedBox(
                height: 15.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
