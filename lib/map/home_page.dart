import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Dashboard"),
        centerTitle: true,
      ),
      drawer: new Drawer(
        child: Container(
          color: Colors.white,
          child: new Column(
            children: <Widget>[
              UserAccountsDrawerHeader(accountName: null, accountEmail: null),
              ListTile(
                leading: Icon(Icons.location_searching),
                title: Text("Find Me"),
                onTap: null, // << Here use location services
              ),
              ListTile(
                leading: Icon(Icons.close),
                title: Text("Log Out"),
                onTap: () {
                  FirebaseAuth.instance.signOut().then((value) {
                    Navigator.of(context).pushReplacementNamed('/landingpage');
                  }).catchError((e) {
                    print(e);
                  });
                },
              ),
            ],
          ),
        ),
      ),
      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text("Login sucessful"),
              SizedBox(
                height: 15.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
